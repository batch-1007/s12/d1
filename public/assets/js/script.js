//Conditional statements are statements that help our program decide what to do
/*
There are three main types of conditional statements
1. if-else statements
2. switch statements
3. try-catch-finally

*/

//If-else statements are used to make decisions based on a condition.
let age = 10;

if (age >= 18) {
	console.log("You may buy wine");
} else {
	console.log("You can not buy wine");
}

/* 
Syntax:
if (conditions){
	do this if the conditions are true
} else {
	do this if the conditions are false
}
*/

//Multiple conditions
let age2 = 19;
let isFullTime = false;

if (age2 >= 18 && !isFullTime){
	console.log("Can enroll to bootcamp day class");
} else {
	console.log("Can enroll to bootcamp night class");
}

/*
What is age2 >= 18? true
What is !isFullTime? true
*/

//If else chaining - this is done to combine several possible outcomes
/* 
if money is greater than 500 - take a grab
if money is between 100 to 500 - take a taxi
if money is between 50 to 100 - take a jeep
if money is between 1 to 50 - walk
else - stay at home
*/

let paoMoney = 100;

if (paoMoney > 500) {
	console.log("Will ride a grab");
} else if (paoMoney > 100 && paoMoney <= 500) { //this is a no no: paoMoney > 100 && <= 500
	console.log("Will ride a taxi");
} else if (paoMoney > 50 && paoMoney <= 100) {
	console.log("Will ride a jeep");
} else if (paoMoney > 1 && paoMoney <= 50) {
	console.log("Will walk");
} else {
	console.log("Stay home");
}

//Create a function that would test if a number is odd or even
function testOddEven(num){
	if (num % 2 == 0){ //num % 2 gets the remainder and we need to compare to 0
		return "even";
	} else {
		return "odd";
	}
}

//Test the function:
console.log(testOddEven(78));
console.log(testOddEven(99523));
console.log(testOddEven(995));

//Mini exercise:
//Create a function that takes two numbers and tests if the difference of the numbers is positive, negative or zero.
//Function name is diffTester
function diffTester(num1, num2){
	let difference = num1 - num2;
	if (difference > 0) {
		return "positive";
	} else if (difference < 0) {
		return "negative";
	} else {
		return "zero";
	}
}
//Test cases:
console.log(diffTester(10, 8));
console.log(diffTester(87, 96));
console.log(diffTester(8, 8));

//create function that would check if the password has 8 characters
function passwordLengthCheck(password){
	if (password.length >= 8){ //.length is a property of strings that count how many characters are there in the string
		return "Password length okay.";
	} else {
		return "Password too short";
	}
}

console.log(passwordLengthCheck("test1234"));
console.log(passwordLengthCheck("1234"));


//Switch statements are used for deciding from discrete values (the values to choose from are distinct)
let directionNumber = 1;

switch(directionNumber){
	case 1:
		console.log("North");
		break;
	case 2:
		console.log("East");
		break;
	case 3:
		console.log("West");
		break;
	case 4:
		console.log("South");
		break;
	default:
		console.log("No matching direction");
}

/*
Syntax:

switch(variable to check){
	case possibleValue1:
		//do something here..
		break;
	case possibleValue2:
		//do something here..
		break;
	...
	default:
		//do something here
}

Each case corresponds to a possible value
The break statement makes sure that the case is finished
The default statement is the fallback if there are no cases that satisfy 
*/

//Create a switch statement that takes a letter (A, B, C) and determines what meal to eat
let choice = "a"; //this will go to the default

switch (choice.toUpperCase()) { //toUpperCase() is a function for strings that convert all lowercase letters to uppercase. //ex. hEllO wORLd -> HELLO WORLD
	case "A":
//	case "a": //you can add more cases as needed and have the same output
		console.log("You have chosen breakfast");
		break;
	case "B":
//	case "b":
		console.log("You have chosen lunch");
		break;
	case "C":
//	case "c":
		console.log("You have chosen dinner");
		break;
	default:
		console.log("No more than 3 meals a day");
}

/*
We can solve it two ways
1. Add another case for lower case letters
2. Manipulate the variable to be upper case at all times
*/

//Mini exercise: create a switch statement that asks for a day (1 to 12) then outputs the corresponding gift. Make sure that there is a default statement.
//ex.
let day = 4;
//output would be calling birds
switch(day){
	case 1: 
		console.log("A partridge in a pear tree");
		break;
	case 2:
		console.log("Two turtle doves");
		break;
	case 3:
		console.log("Three french hens");
		break;
	case 4:
		console.log("Four calling birds");
		break;
	case 5: 
		console.log("Five gold rings");
		break;
	case 6:
		console.log("Six geese-a-laying");
		break;
	case 7: 
		console.log("Seven swans a-swimming");
		break;
	case 8:
		console.log("Eight maids a-milking");
		break;
	case 9:
		console.log("Nine ladies dancing");
		break;
	case 10: 
		console.log("Ten lords a-waiting");
		break;
	case 11:
		console.log("Eleven pipers piping");
		break;
	case 12:
		console.log("Twelve drummers drumming");
		break;
	default:
		console.log("No corresponding gifts");
}

//try-catch-finally statement is used to run codes and catch error messages
try {
	//code to try executing, this part would be the code that has a chance of failing
	//examples would be alerts, computations, opening files
	alerat("Hello World"); //for now this is intentional
} catch (err) { 
	//err is a variable that catches the error, it is user defined and can have whatever name you want
	console.log(err.message); 
	//.message is a property that shows what error message was encountered
} finally {
	alert ("This runs regardless if the try block succeeds or fails.");
	//any code put in the finally block will run regardless if the try succeeds or fails
}

//Ternary Operator -> a shorthand of our if-else statement
/* Syntax:
if else statement
if (condition/s) {
	do this if the conditions are true	
} else {
	do this if the conditions are false
}

ternary statement
(condition) ? value if true : value if false
Note however that it only outputs values
*/

//Recall code for odd even test?
let numToTest = 67;
if (numToTest % 2 == 0) {
	console.log("even");
} else {
	console.log("odd");
}

function oddEvenTest2(number){
	return (number % 2 == 0) ? "even" : "odd";
}

console.log(oddEvenTest2(numToTest));

//Mini exercise:
//Create a function that gets a number and doubles it if it is odd and halves it if it is even.
/* 
Test cases:
15 -> 30
656 -> 328
99 -> 198
80 -> 40
*/
function doubleHalf(num){
	if(num % 2 == 0){
		return num / 2;
	} else {
		return num * 2;
	}
}

console.log(doubleHalf(15));
console.log(doubleHalf(656));

//Create a function that gets two numbers and a letter (a and b), if "a" is chosen, add the two numbers, if "b" is chosen, subtract the two numbers
/*
(1, 1, "a") -> 2
(7, 3, "b") -> 4
(7, 8, "c") -> "Invalid letter"
*/

function addSub(numA, numB, letter){
	let letterCleaned = letter.toLowerCase();
	if (letterCleaned == "a"){
		return numA + numB;
	} else if (letterCleaned == "b"){
		return numA - numB;
	} else {
		return "Invalid letter";
	}
}

console.log(addSub(1, 1, "a"));
console.log(addSub(7, 3, "b"));
